# Coalition Technologies Starter Theme
This theme was based on underscores.me and lays the foundation for a fast site with the best possible Page Speed results. Has a .gitignore file to exclude WP core files and media files from being version controlled.

In the root of the theme, there is a [Prepros](https://prepros.io/downloads) configuration file. Prepros is a software for compling SCSS into CSS and minifying JS. Supported for Windows, Mac and Linux.

If it's the first time that you're using this theme, it's recommended that you read this first and then read the code of the theme before developing.

## Plugin List
```
- Advanced Custom Fields Pro 5.8.12
- Gravity Forms 2.4.18
- Post Types Order 1.9.4.3
- Query Monitor 3.6.0
- Redirection 4.8
- WooCommerce 4.2.0
- Yoast SEO 14.3
- Yoast SEO WooCommerce 13.2
```

Once the site is ready for production, install the following plugins for better performance
```
- Autoptimize
- WP Smush
```
## Theme Information
The theme disables certain core WordPress features to make the site lightweight.
```
- Disables WP Heartbeat
- Removes Unneeded dashboard widgets
- Disables Really Simple Discovery pingback
- Disables Windows Live Writer
- Disables Emoticons
- Removes WP generated Shortlinks
- Disable oEmbeds
- Disables XML-RPC as a security precaution
- Removes WP version from the <head> as a security precaution
- Removes WLManifest Link
- Disables pingbacks
```

### Custom Functions
PHP functions were created to make it easier to develop. All custom functions should be added to `/inc/helper-functions.php`.

- `assets_directory();` Returns the thems's assets directory path.
- `assets_uri();` Returns the theme's assets URL path.
- `ct_enqueue_scripts( $scripts = array() );` Easier way to enqueue JS, check `/inc/helper-functions.php line 61` for more info.
- `ct_logo();` Echo the logo set from the theme settings page (SVG or PNG/JPG/GIF).
- `is_really_woocommerce_page();` Returns true or false if the page is really a WooCommerce page.
- `ct_posted_on();` Echos the date the post or page was posted on.
- `ct_posted_by();` Echos the author of the post or page.
- `ct_block_init( $class = string );` Echos the ID and class for custom ACF Block Type.

### Shortcodes
- `[ct-sitemap]` Echos the pages, posts or any custom post type which don't have noindex set on them from Yoast SEO. REQUIRES YOAST SEO TO BE ACTIVE FOR IT TO WORK!!

### Page Builder
This theme encourages the use of ACF Gutenberg Blocks instead of Custom Fields whenever possible because it requires less development time for future pages and it's keeping up with the trend.

All blocks should go into the `/acf/` folder. In the `/acf/init.php` and `/acf/templates/test.php` you'll find examples of how to create an ACF block type.