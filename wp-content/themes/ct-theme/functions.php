<?php
/**
 * Coalition Technologies functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Coalition_Technologies
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ct_setup() {

// Add theme support for
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script'
		)
	);

	add_theme_support( 'title-tag' );
	add_theme_support( 'woocommerce' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'customize-selective-refresh-widgets' );

// Register main navigation area
	register_nav_menus( array(
			'primary' => 'Primary',
	) );

// Register widget areas
	register_sidebar(
		array(
			'name'          => 'Footer Column 1',
			'id'            => 'footer-1',
			'description'   => 'Add widgets here.',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="footer-title">',
			'after_title'   => '</h3>',
		)
	);
	register_sidebar(
		array(
			'name'          => 'Footer Column 2',
			'id'            => 'footer-2',
			'description'   => 'Add widgets here.',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="footer-title">',
			'after_title'   => '</h3>',
		)
	);
	register_sidebar(
		array(
			'name'          => 'Footer Column 3',
			'id'            => 'footer-3',
			'description'   => 'Add widgets here.',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="footer-title">',
			'after_title'   => '</h3>',
		)
	);
	register_sidebar(
		array(
			'name'          => 'Footer Column 4',
			'id'            => 'footer-4',
			'description'   => 'Add widgets here',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="footer-title">',
			'after_title'   => '</h3>',
		)
	);

	register_sidebar(
		array(
			'name'          => 'Sidebar',
			'id'            => 'sidebar-1',
			'description'   => 'Add widgets here.',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="sidebar-title">',
			'after_title'   => '</h3>',
		)
	);

}
add_action( 'after_setup_theme', 'ct_setup' );


/**
 * Enqueue scripts and styles.
 */
function ct_scripts() {
	/**
	 * Custom enqueuing for scripts
	 *
	 *  Handle => array (
	 *      path (/ct-theme/assets/js/ is the path used),
	 *      version (if empty will use filemtime),
	 *      require all prior scripts (true, false (only require jQuery), define script handles that are required)
	 *  )
	 */
	$scripts = array(
		'main' => array( 'main.js', '', false ),
	);

	ct_enqueue_scripts( $scripts );

// For all pages.
	$cache = filemtime( assets_directory() . '/css/main.css' );
	wp_enqueue_style( 'ct-main', assets_uri() . '/css/main.css', false, $cache, 'all' );

// Assets for front page.
	if ( is_front_page() ) {
		$cache = filemtime( assets_directory() . '/css/home.css' );
		wp_enqueue_style( 'ct-home', assets_uri() . '/css/home.css', false, $cache, 'all' );
	}

// Assets for default page template.
	if ( basename( get_page_template() ) === 'page.php' && ! is_front_page() ) {
		$cache = filemtime( assets_directory() . '/css/page.css' );
		wp_enqueue_style( 'ct-page', assets_uri() . '/css/page.css', false, $cache, 'all' );
	}

// Assets for single post templates.
	if ( is_singular() ) {
		$cache = filemtime( assets_directory() . '/css/single.css' );
		wp_enqueue_style( 'ct-single', assets_uri() . '/css/single.css', false, $cache, 'all' );
	}

// Assets for archive templates.
	if ( is_post_type_archive() || is_archive() || is_home() ) {
		$cache = filemtime( assets_directory() . '/css/archives.css' );
		wp_enqueue_style( 'ct-archives', assets_uri() . '/css/archives.css', false, $cache, 'all' );
	}

// Assets for any WooCommerce page template
	if ( is_realy_woocommerce_page() ) {

	}

}
add_action( 'wp_enqueue_scripts', 'ct_scripts' );



/**
 * File for ACF theme settings
 */
require get_template_directory() . '/acf/init.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/helper-functions.php';

/**
 * Custom shortcodes for the theme.
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Cleaning up the site.
 */
require get_template_directory() . '/inc/cleanup.php';

/**
 * WooCommerce integration.
 */
require get_template_directory() . '/inc/woocommerce/init.php';

