<?php

/**
 * Function to create ID and classes for block type.
 * 
 * @param	string	$class		Custom classes to append.
 * @return	string				ID and class for block element
 */
function ct_block_init( $class = '' ) {

	// The array of arguments for registering a block type.
	global $ctblock;

	if ( !empty( $ctblock ) ) :

		$classes = '';

		if ( !empty( $ctblock['align'] ) ) :
			$classes .= 'align-'.$ctblock['align'] . ' ';
		endif;

		if ( !empty( $ctblock['className'] ) ) :
			$classes .= trim( $ctblock['className'] ) . ' ';
		endif;

		if ( $class !== '' ) :
			$classes .= trim( $class );
		endif;


		$id = str_replace( 'acf/', '', $ctblock['name'] ) . str_replace( 'block_', '-',$ctblock['id'] );
		if ( $ctblock['anchor'] ) :
			$id = $ctblock['anchor'];
		endif;


		echo ' id="' . $id . '" class="' . esc_attr( $classes ) . '"';

	endif;

}

/**
 * Get direct assets directory path
 *
 * @return	string
 */
function assets_directory() {
	return get_template_directory() . '/assets';
}

/**
 * Get the assets URL path
 *
 * @return	string
 */
function assets_uri() {
	return get_template_directory_uri() . '/assets';
}

/**
 * This function is for simplifying the enqueue process of JS
 *
 * @param	array of info about script
 *  Handle => array (
 *      path (used from theme assets),
 *      version (if empty will use filemtime),
 *      require all prior scripts (true, false (only require jQuery), define required)
 *  )
 *
 * @return	wp_enqueue_script
 */
function ct_enqueue_scripts( $scripts ) {
	foreach ( $scripts as $i => $script ) {
		$ver = ( $script[1] !== '' ) ? $script[1] : filemtime( assets_directory() . '/js/' . $script[0] );
		if ( ! $script[2] ) {
			$require = array( 'jquery' );
		} elseif ( $script[2] ) {
			$n       = array_keys( $scripts );
			$count   = array_search( $i, $n );
			$require = array_keys( array_slice( $scripts, 0, $count, true ) );
		} else {
			$require = $script[2];
		}
		wp_enqueue_script( $i, assets_uri() . '/js/' . $script[0], $require, $ver );
	}
}

/**
 *  Function for outputting the logo
 *
 * @return	logo (PNG or SVG)
 */
function ct_logo() {
	if ( get_field( 'logo_type', 'option' ) ) {
		the_field( 'logo_svg', 'option' );
	} else {
		echo wp_get_attachment_image( get_field( 'logo_media', 'option' ), 'full' );
	}
}

/**
* Returns true if on a page which uses WooCommerce templates (cart and checkout are standard pages with shortcodes and which are also included)
*
* @return bool
*/
function is_realy_woocommerce_page() {
    if( function_exists ( "is_woocommerce" ) && is_woocommerce()){
        return true;
    }
    $woocommerce_keys = array ( "woocommerce_shop_page_id" ,
        "woocommerce_terms_page_id" ,
        "woocommerce_cart_page_id" ,
        "woocommerce_checkout_page_id" ,
        "woocommerce_pay_page_id" ,
        "woocommerce_thanks_page_id" ,
        "woocommerce_myaccount_page_id" ,
        "woocommerce_edit_address_page_id" ,
        "woocommerce_view_order_page_id" ,
        "woocommerce_change_password_page_id" ,
        "woocommerce_logout_page_id" ,
        "woocommerce_lost_password_page_id" ) ;

    foreach ( $woocommerce_keys as $wc_page_id ) {
        if ( get_the_ID () == get_option ( $wc_page_id , 0 ) ) {
            return true ;
        }
    }
    return false;
}

/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Coalition_Technologies
 */

if ( ! function_exists( 'ct_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function ct_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
		}

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
		);

		echo '<span class="posted-on">' . $time_string . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'ct_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function ct_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'ct' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'ct_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function ct_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'ct' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'TAGS %1$s', 'ct' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'ct' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}
	}
endif;

if ( ! function_exists( 'ct_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function ct_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
			<?php
			the_post_thumbnail(
				'post-thumbnail',
				array(
					'alt' => the_title_attribute(
						array(
							'echo' => false,
						)
					),
				)
			);
			?>
		</a>

			<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'wp_body_open' ) ) :
	/**
	 * Shim for sites older than 5.2.
	 *
	 * @link https://core.trac.wordpress.org/ticket/12563
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
endif;
