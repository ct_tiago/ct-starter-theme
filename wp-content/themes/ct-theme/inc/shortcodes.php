<?php

/**
 * Shortcode for HTML Sitemap
 * Does not unindex blog posts
 *
 * @param  array
 * @return string of HTML sitemap
 */
function ct_sitemap( $atts ) {

	$output = '';
	foreach ( get_post_types( array( 'public' => 1 ), 'names', 'and' ) as $type ) {
		if ( $type !== 'attachment' && WPSEO_Post_Type::is_post_type_indexable( $type ) ) {

			$query = new WP_Query(
				array(
					'posts_per_page'    => -1,
					'post_type'         => $type,
					'meta_query'        => array(
						'relation'      => 'OR',
						array(
							'compare'   => 'NOT EXISTS',
							'key'       => '_yoast_wpseo_meta-robots-noindex',
						),
						array(
							'value'     => 1,
							'compare'   => '!=',
							'key'       => '_yoast_wpseo_meta-robots-noindex',
						),
					),
				)
			);
			if ( $query->have_posts() ) {
				$output .= '<h3>' . get_post_type_object( $type )->labels->name . '</h3>';

				$output .= '<ul>';
				if ( get_post_type_object( $type )->hierarchical ) {
					$output .= wp_list_pages(
						array(
							'echo'      => 0,
							'title_li'  => null,
							'post_type' => $type,
							'include'   => wp_list_pluck( $query->posts, 'ID' ),
						)
					);
				} else {
					while ( $query->have_posts() ) {
						$query->the_post();
						$output .= '<li><a href="' . get_the_permalink( get_the_ID() ) . '">' . get_the_title() . '</a></li>';
					}
				}
				$output .= '</ul>';
			}
			wp_reset_postdata();

		}
	}

	return $output;
}
add_shortcode( 'ct-sitemap', 'ct_sitemap' );
